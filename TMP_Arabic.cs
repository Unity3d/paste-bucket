﻿using UnityEngine;
using TMPro;

public class TMP_Arabic : MonoBehaviour {

    private string txtToDisplay;
    private TMP_Text txtToComponent;

    void Start ()
    {
        //Get the Text Mesh Pro Text Component
        txtToComponent = GetComponent<TMP_Text>();

        //Get Text Input Box
        txtToDisplay = txtToComponent.text;

        //Enable RTL Text feature in TMP
        txtToComponent.isRightToLeftText = true;

        //Use Arabic Support to fix the text
        txtToComponent.text = ArabicSupport.ArabicFixer.Fix
                                (txtToDisplay, true, true);

        //Finally reverst the text
        txtToComponent.text = ReverseText(txtToComponent.text);
    }

    string ReverseText(string source)
    {
        char[] output = new char[source.Length];
        for (int i = 0; i < source.Length; i++)
        {
            output[(output.Length - 1) - i] = source[i];
        }
        return new string(output);
    }
}
